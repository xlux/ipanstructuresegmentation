from typing import Tuple

TypePairFloat = Tuple[float, float]
TypeTripletFloat = Tuple[float, float, float]
TypeSextetFloat = Tuple[float, float, float, float, float, float]
