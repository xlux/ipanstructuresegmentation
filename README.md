# IPAN Segmentation


used libraries:
- **Pytorch** - tensor computation 
- **MLFlow** - visualisation and logging of a training process
- **Napari** - 3D visualisation
- **Bio-Volumentations** - augmentation of training data

## prerequisities

- [ ] pytorch, torchvision https://pytorch.org/get-started/locally/

## get started 

#### 1. Create Conda Environment **ipan-env**


``` bash
conda create -n ipan-env python=3.10 
conda activate ipan_env 
```

#### 2. Install *napari*

The last command should run the napari image browser.
For more information see the napari documentations.
 https://napari.org/stable/tutorials/fundamentals/installation

``` bash
# install napari
conda install pyside2 -c conda-forge
python -m pip install "napari[pyside2]"
napari
```

#### 3. Install Other Packages
``` bash
python -m pip install lightning[extra] matplotlib numpy dipy simpleitk mlflow bio-volumentations

# pytorch library needs to be installed in a specific version
python -m pip install torch==2.0.1 torchvision==0.15.2
```

#### 4. (optional)  Install *jupyter lab* 


``` bash
conda install jupyterlab ipykernel
python -m ipykernel install --user --name=IPAN_env
jupyter lab
```


