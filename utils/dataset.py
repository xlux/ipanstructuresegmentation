# -*- coding: utf-8 -*-


import numpy as np
import torch
from torch.utils.data import Dataset

from biovol.core.composition import Compose
from biovol.augmentations.transforms import (RandomAffineTransform,
                                             RandomGaussianBlur,
                                             RandomBrightnessContrast,
                                             PoissonNoise,
                                             CenterCrop)

import random


def get_template_sample(shape,
                        diameter: int,
                        axis=2) -> dict:
    # find center
    shape_mod = list(shape)
    shape_mod[axis] = 1

    x, y, z = shape_mod
    center = (x - 1) / 2, (y - 1) / 2, (z - 1) / 2

    # compute distance map
    xx, yy, zz = np.ogrid[:x, :y, :z]
    dist_from_center = np.sqrt((xx - center[0]) ** 2 +
                               (yy - center[1]) ** 2 +
                               (zz - center[2]) ** 2)

    # create binary structure
    mask = dist_from_center < (diameter / 2) + .5
    tunnel = np.repeat(mask, shape[axis], axis=axis)

    return {'image': np.expand_dims(tunnel, axis=0),
            'mask': np.expand_dims(tunnel, axis=0)}


def get_augmentation():
    return Compose([
        RandomAffineTransform(angle_limit=[0, 0, -30, 30, 0, 180],
                              scaling_limit=[.6, 2., .6, 2., .6, 2.],
                              p=1),
        RandomGaussianBlur(max_sigma=2.5, p=1),
        # RandomBrightnessContrast(brightness_limit=(0, .5), contrast_limit=0)
        PoissonNoise(intensity_limit=(0.02, 0.02), p=1)
    ],
        p=1.0,
        targets=[['image'], ['mask'], []]
    )


def get_augmentation_pn(angles_limit=(30, 30, 180),
                        scale_limit=(1., 1., 1.),
                        translation_limit=(10, 10, 5),
                        sigma=(0, 1),
                        intensity=.5,
                        contrast_limit=(0, 1),
                        shape=(24, 24, 12)):
    return Compose([
        RandomAffineTransform(angle_limit=angles_limit,
                              scaling_limit=scale_limit,
                              translation_limit=translation_limit,
                              p=1),
        RandomGaussianBlur(max_sigma=sigma[1],
                           start_of_interval=sigma[0],
                           p=1),
        RandomBrightnessContrast(brightness_limit=0,
                                 contrast_limit=contrast_limit,
                                 p=1),
        PoissonNoise(intensity_limit=(intensity, intensity),
                     p=1),
        CenterCrop(shape)
    ],
        p=1.0,
        targets=[['image'], ['mask'], []]
    )


class SyntheticTunnelsDataset(Dataset):

    def __init__(self,
                 sample_shape=(12, 12, 6),
                 n_samples=1000,
                 diameter=2,
                 validation=True,
                 negative_label=2):

        self.sample_shape = sample_shape
        self.validation = validation
        self.size = n_samples
        self.negative_label = negative_label

        extended_shape = np.array(sample_shape) * 2

        self.aug_pos = get_augmentation_pn(angles_limit=(0, 0, -30, 30, 0, 180),
                                           scale_limit=(.5, 1.8),
                                           sigma=(.5, 2),
                                           intensity=0.05,
                                           contrast_limit=(0.5, .8),
                                           shape=sample_shape)

        self.aug_neg = get_augmentation_pn(angles_limit=(-40, 40, -40, 40, 0, 0),
                                           scale_limit=(1, 3),
                                           sigma=(2, 5),
                                           intensity=0.06,
                                           contrast_limit=(-0.8, -0.3),
                                           shape=sample_shape)

        self.template_pos = get_template_sample(extended_shape,
                                                1,
                                                0)
        self.template_neg = get_template_sample(extended_shape,
                                                5,
                                                2)

        self.stable_samples = []

        if self.validation:
            # set the state
            n_pos = self.size // 2
            n_neg = self.size - n_pos

            random.seed(42)
            for _ in range(n_pos):
                self.stable_samples.append(self.aug_pos(**self.template_pos))
            for _ in range(n_neg):
                aug_data = self.aug_neg(**self.template_neg)
                aug_data['mask'] = aug_data['mask'] * self.negative_label
                self.stable_samples.append(aug_data)

    def __len__(self):
        return self.size

    def __getitem__(self, idx):

        if self.validation:
            aug_data = self.stable_samples[idx]
        else:
            if idx % 2 == 0:
                # positive sample
                aug_data = self.aug_pos(**self.template_pos)
            else:
                # negative sample
                aug_data = self.aug_neg(**self.template_neg)
                aug_data['mask'] = aug_data['mask'] * self.negative_label

        return (torch.from_numpy(aug_data['image']).float(),
                torch.from_numpy(aug_data['mask'].squeeze()).long())

