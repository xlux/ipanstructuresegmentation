from utils.unet import SmallUNet
import lightning as L
from torch import nn
import torchmetrics
import torch
import numpy as np


class TunnelDetector(L.LightningModule):
    def __init__(self,
                 weights=(0.1, 1.0)):
        super().__init__()

        n_classes = len(weights)
        
        self.model = SmallUNet(n_classes=n_classes)
        self.loss_func = nn.CrossEntropyLoss(torch.tensor(weights))
        self.j_index = torchmetrics.JaccardIndex(task="multiclass", num_classes=n_classes)
        self.train_epoch_loss = 0
        self.train_epoch_jaccard = 0
        self.validation_epoch_loss = 0
        self.validation_epoch_jaccard = 0
        self.test_epoch_loss = 0
        self.test_epoch_jaccard = 0
        self.validation_loss_history = []
        self.train_samples_seen = 0
        self.validation_samples_seen = 0
        self.last_y = None
        self.last_prediction = None

    def forward(self, inputs):
        return self.model(inputs)

    def training_step(self, batch, idx):
        # get_sample
        x, y = batch

        pred = self.model(x)
        loss = self.loss_func(pred, y).cpu()

        # store loss
        batch_size = len(x)
        self.train_samples_seen += batch_size
        self.train_epoch_loss += loss * batch_size
        self.train_epoch_jaccard += self.j_index(pred, y).cpu() * batch_size

        return loss

    def validation_step(self, batch, idx):
        # get sample
        x, y = batch
        prediction = self.model(x)
        loss = self.loss_func(prediction, y).cpu()

        # store loss
        batch_size = len(x)
        self.validation_samples_seen += batch_size
        self.validation_epoch_loss += loss * batch_size
        self.validation_epoch_jaccard += self.j_index(prediction, y).cpu() * batch_size

        self.last_prediction = prediction.cpu()
        self.last_y = y.cpu()

    def on_validation_epoch_end(self) -> None:

        val_loss = self.validation_epoch_loss / self.validation_samples_seen

        self.validation_epoch_loss = self.validation_samples_seen = 0
        self.log("val_loss", val_loss)


        if self.train_samples_seen > 0:
            loss = self.train_epoch_loss / self.train_samples_seen

            self.train_epoch_loss = self.train_samples_seen = 0
            self.log("loss", loss)
            print('loss', loss, 'val_loss', val_loss)
        else:
            print('train loss was 0')

    def test_step(self, batch, batch_idx):
        x = batch
        prediction = self.forward(x)

        # TODO: store results

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=0.001)
        return optimizer
