# -*- coding: utf-8 -*-


import os
from typing import Any
from lightning.pytorch.utilities.types import STEP_OUTPUT
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
import nibabel as nib

import numpy as np
import torchmetrics
import torch
import mlflow
from torch import nn


a = 0
b = 0
device = (
        "cuda"
        if torch.cuda.is_available()
        else "cpu"
)


class EncoderBlock(nn.Module):
    def __init__(self, number_of_channels, 
                 kernel_size=3, 
                 maxpool_size=2, 
                 last_block=False):
        """ number_of_channels : 2 value list containing the number of channels,
                                first value: number of input channels for current block
                                second value : number of output channels for first layer
                                in block"""
        super().__init__()

        self.block = nn.Sequential(
            nn.Conv3d(in_channels=number_of_channels[0],
                      out_channels=number_of_channels[1],
                      kernel_size=kernel_size,
                      padding="same"),
            nn.ReLU(),
            nn.BatchNorm3d(number_of_channels[1]),
            nn.Conv3d(in_channels=number_of_channels[1],
                      out_channels=number_of_channels[2],
                      kernel_size=kernel_size,
                      padding="same"),
            nn.ReLU(),
            nn.BatchNorm3d(number_of_channels[2])
        ).float()
        self.is_last = last_block
        self.pooler = nn.MaxPool3d(kernel_size=2).float()
        self.upConv = nn.ConvTranspose3d(in_channels=number_of_channels[2],
                                         out_channels=number_of_channels[2],
                                         kernel_size=2,
                                         stride=2,
                                         padding=0,
                                         output_padding=0
                                         )

    def forward(self, input_x):
        convolution_res = self.block(input_x)
        if not self.is_last:
            maxpooled_res = self.pooler(convolution_res)
            return maxpooled_res, convolution_res
        return self.upConv(convolution_res)


class DecoderBlock(nn.Module):
    def __init__(self, number_of_channels, kernel_size=3, last_block=False):
        super().__init__()

        self.block = nn.Sequential(
            nn.Conv3d(in_channels=number_of_channels[0],
                      out_channels=number_of_channels[1],
                      kernel_size=kernel_size,
                      padding="same"),
            nn.ReLU(),
            nn.BatchNorm3d(number_of_channels[1]),
            nn.Conv3d(in_channels=number_of_channels[1],
                      out_channels=number_of_channels[1],
                      kernel_size=kernel_size,
                      padding="same"),
            nn.ReLU(),
            nn.BatchNorm3d(number_of_channels[1])
        ).float()
        
        self.is_last = last_block
        self.upConv = nn.ConvTranspose3d(in_channels=number_of_channels[1],
                                         out_channels=number_of_channels[1],
                                         kernel_size=2,
                                         stride=2,
                                         padding=0,
                                         output_padding=0
                                         )

    def forward(self, input_x, encoder_output):
        if self.is_last:
            return self.block(torch.cat((encoder_output, input_x), dim=1))
        return self.upConv(self.block(torch.cat((encoder_output, input_x), dim=1)))


class SmallUNet(nn.Module):
    def __init__(self,
                 n_classes=2):
        super().__init__()
        self.encoder_block1 = EncoderBlock(number_of_channels=[1, 32, 64]).to(device)
        self.encoder_block2 = EncoderBlock(number_of_channels=[64, 64, 64], last_block=True).to(device)
        self.decoder_block1 = DecoderBlock(number_of_channels=[64 + 64, 32], last_block=True).to(device)
        self.last_conv = nn.Conv3d(in_channels=32,
                                   out_channels=n_classes,
                                   kernel_size=1,
                                   padding="same").to(device)
        self.softmax = nn.Softmax(dim=1).to(device)

    def forward(self, input_x):
        x = input_x
        x, y1 = self.encoder_block1(x)
        x = self.encoder_block2(x)
        x = self.decoder_block1(x, y1)
        res = self.last_conv(x)

        return res


