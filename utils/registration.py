from dipy.align.imwarp import SymmetricDiffeomorphicRegistration
from dipy.align.metrics import CCMetric
from dipy.align.transforms import AffineTransform3D
from dipy.align.imaffine import MutualInformationMetric, AffineRegistration

import numpy as np


def get_nonrigid_transform(static,
                           moving,
                           level_iters=(10, 10, 5)):
    
    static_affine = moving_affine = pre_align = np.identity(4)
    
    metric = CCMetric(3, radius=2)
    sdr = SymmetricDiffeomorphicRegistration(metric, level_iters)
    return sdr.optimize(static, moving, static_affine, moving_affine, pre_align)


def get_affine_transform(static, moving):
    nbins = 32
    sampling_prop = None
    metric = MutualInformationMetric(nbins, sampling_prop)
    level_iters = [10000, 1000, 100]
    sigmas = [3.0, 1.0, 0.0]
    factors = [4, 2, 1]

    affreg = AffineRegistration(metric=metric,
                                level_iters=level_iters,
                                sigmas=sigmas,
                                factors=factors)

    static_affine = moving_affine = starting_affine = np.identity(4)

    transform = AffineTransform3D()
    params0 = None
    affine = affreg.optimize(static, moving, transform, params0,
                             static_affine, moving_affine,
                             starting_affine=starting_affine)
    
    return affine
