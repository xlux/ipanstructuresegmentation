import torch
import numpy as np
from biovol import (Compose,
                    RandomAffineTransform,
                    RandomGaussianBlur,
                    RandomBrightnessContrast,
                    GaussianNoise,
                    PoissonNoise,
                    CenterCrop)



def get_augmentation_pn(angles_limit=(30, 30, 180),
                        scale_limit=(1., 1., 1.),
                        sigma=(0, 1),
                        intensity=.5,
                        contrast_limit=(0, 1),
                        shape=(24, 24, 12)):
    
    
    
    return Compose([
        RandomAffineTransform(angle_limit=angles_limit,
                              scaling_limit=scale_limit,
                              p=1),
        RandomGaussianBlur(max_sigma=sigma[1],
                           start_of_interval=sigma[0],
                           p=1),
        RandomBrightnessContrast(brightness_limit=0,
                                 contrast_limit=contrast_limit,
                                 p=1),
        PoissonNoise(intensity_limit=(intensity, intensity),
                     p=1),
        CenterCrop(shape)
    ],
        p=1.0,
        targets=[['image'], ['mask'], []]
    )


def get_template_sample(shape,
                        diameter: int,
                        axis=2) -> dict:

    # find center
    shape_mod = list(shape)
    shape_mod[axis] = 1
    
    x, y, z = shape_mod
    center = (x-1)/2, (y-1)/2, (z-1)/2
        
    # compute distance map
    xx, yy, zz = np.ogrid[:x, :y, :z]
    dist_from_center = np.sqrt((xx - center[0])**2 +
                               (yy - center[1])**2 +
                               (zz - center[2])**2)

    # create binary structure
    mask = dist_from_center < (diameter / 2) + .5
    tunnel = np.repeat(mask, shape[axis], axis=axis)
            
    return {'image': np.expand_dims(tunnel, axis=0),
            'mask': np.expand_dims(tunnel, axis=0)}
