import numpy as np
import napari
from matplotlib import pyplot as plt
from skimage import exposure


def show_sample_np(img_np,
                   colormap=('green', 'red'),
                   scale=(1, 4, 1, 1),
                   rescale_intensity=True):

    viewer = napari.Viewer(ndisplay=3)
    
    n_dims = len(img_np.shape)
    if n_dims == 3:
        img_np = np.expand_dims(np.expand_dims(img_np, axis=0), axis=-1)
    elif (n_dims == 4) or (n_dims > 5) or (n_dims < 3):
        print(f'Warning: cannot decide about the image shape {img_np.shape}')
        return

    for c in range(img_np.shape[0]):

        im = img_np[c]

        if rescale_intensity:
            pL, pH = np.percentile(img_np, (.5, 99.5))
            im = exposure.rescale_intensity(im, in_range=(pL, pH))

        im = np.moveaxis(im, -2, 0)
        im = np.moveaxis(im, -1, 0)

        viewer.add_image(im, name=f'ch{c}')
        viewer.layers[f'ch{c}'].interpolation = 'nearest'
        viewer.layers[f'ch{c}'].interpolation4d = 'nearest'
        viewer.layers[f'ch{c}'].interpolation2D = 'nearest'
        viewer.layers[f'ch{c}'].scale = scale
        viewer.layers[f'ch{c}'].blending = 'additive'
        viewer.layers[f'ch{c}'].colormap = colormap[c % len(colormap)]
        

def show_one_sample(sample,
                    colormap=('green', 'red', 'blue'),
                    scale=(1, 4, 1, 1),
                    rescale_intensity=True,
                    gt_path=None,
                    translate=(50, 50, 50)):
    
    img = sample['image']
    mask = sample['mask']
    
    img_np = np.expand_dims(np.concatenate([img*1, mask*1], axis=0), axis=-1)
    
    viewer = napari.Viewer(ndisplay=3)
    
    n_dims = len(img_np.shape)
    if n_dims == 3:
        img_np = np.expand_dims(np.expand_dims(img_np, axis=0), axis=-1)
    elif (n_dims == 4) or (n_dims > 5) or (n_dims < 3):
        print(f'Warning: cannot decide about the image shape {img_np.shape}')
        return

    for c in range(img_np.shape[0]):

        im = img_np[c]
        print(f'channel {c}', im.shape)
        
        if rescale_intensity:
            pL, pH = np.percentile(img_np, (.5, 99.5))
            im = exposure.rescale_intensity(im, in_range=(pL, pH))

        im = np.moveaxis(im, -2, 0)
        im = np.moveaxis(im, -1, 0)

        viewer.add_image(im,
                         name=f'ch{c}',
                         scale=scale,
                         interpolation3d='nearest',
                         blending='additive',
                         colormap=colormap[c % len(colormap)])
        
    if gt_path is not None:
        # load GT

        im = np.load(gt_path)
        channels = np.split(im, (1, 2), axis=0)
        im = channels[0][0, ..., 0:1]
        
        print('gt', im.shape)
        
        im = np.moveaxis(im, -2, 0)
        im = np.moveaxis(im, -1, 0)

        viewer.add_image(im,
                         name='gt',
                         scale = scale,
                         interpolation3d='nearest',
                         blending = 'additive',
                         colormap = 'green',
                         translate=translate)
        
           
def concatenate_samples(samples: list,
                        axis: int = 1) -> dict:
    
    imgs, masks = [], []
    for sample in samples:
        imgs.append(sample['image'])
        masks.append(sample['mask'])
        
    return {'image': np.concatenate(imgs, axis=axis),
            'mask': np.concatenate(masks, axis=axis)}
    

        